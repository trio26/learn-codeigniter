<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function index()
    {

        $data['title'] = 'Dashboard';
        $data['another_title'] = ' User';
        $data['login'] = $this->db->get_where('register', ['email' =>
        $this->session->userdata('email')])->row_array();

        $data['data'] = $this->db->get('register')->result_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/index', $data);
        $this->load->view('templates/footer');
    }

    public function Role()
    {

        $data['title'] = 'Role';
        $data['login'] = $this->db->get_where('register', ['email' =>
        $this->session->userdata('email')])->row_array();
        $data['role'] = $this->db->get('user_role')->result_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/role', $data);
        $this->load->view('templates/footer');
    }

    public function roleaccess($role_id)
    {

        $data['title'] = 'Role Access';
        $data['login'] = $this->db->get_where('register', ['email' =>
        $this->session->userdata('email')])->row_array();

        $this->db->where('id != ', 1);
        $data['role'] = $this->db->get_where('user_role', ['id' => $role_id])->row_array();

        $data['menu'] = $this->db->get('user_menu')->result_array();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/roleaccess', $data);
        $this->load->view('templates/footer');
    }

    public function edit()
    {

        $data['title'] = 'Edit User';
        $data['login'] = $this->db->get_where('register', ['email' =>
        $this->session->userdata('email')])->row_array();

        $data['data'] = $this->db->get('register')->result_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/edit_user', $data);
        $this->load->view('templates/footer');
    }
    public function hapus($id)
    {
        $this->db->delete('register', array('customer_id' => $id));
        // $this->db->get_where('register', ['customer_id' => $id])->row_array;
        // $this->db->delete('register');
        $this->session->set_flashdata(
            'message',
            '<div class="alert alert-success" role="alert">
			Delete user success !</div>'
        );
        redirect('admin');
    }
}
