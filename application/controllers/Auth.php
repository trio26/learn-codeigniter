<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Auth extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
	}

	public function index()
	{
		if ($this->session->userdata('email')) {
			redirect('user');
		}

		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required|trim');

		if ($this->form_validation->run() == false) {
			$data['title'] = 'User login';
			$this->load->view('templates/auth_header', $data);
			$this->load->view('auth/login');
			$this->load->view('templates/auth_footer');
		} else {
			$this->_login();
		}
	}

	private function _login()
	{

		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$login = $this->db->get_where('register', ['email' => $email])->row_array();


		// echo "<pre>";
		// print_r($this->input->post());
		// exit;
		// jika usernya ada
		if ($login) {

			if ($login['is_active'] == 1) {

				if (password_verify($password, $login['password'])) {

					$data = [
						'email' => $login['email'],
						'role_id' => $login['role_id']
					];

					$this->session->set_userdata($data);

					if ($login['role_id'] == 1) {
						redirect('admin');
					} else {
						redirect('user');
					}
				} else {

					$this->session->set_flashdata(
						'message',
						'<div class="alert alert-danger" role="alert">
					Your password is wrong ! ! </div>'
					);
					redirect('auth');
				}
			} else {

				$this->session->set_flashdata(
					'message',
					'<div class="alert alert-danger" role="alert">
				Your email is not registered ! </div>'
				);
				redirect('auth');
			}
		} else {
			// jika belum daftar
			$this->session->set_flashdata(
				'message',
				'<div class="alert alert-danger" role="alert">
			Your email is not registered ! </div>'
			);
			redirect('auth');
		}
	}

	public function registration()
	{
		if ($this->session->userdata('email')) {
			redirect('user');
		}

		$this->form_validation->set_rules('name', 'Name', 'required|trim');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[register.email]');
		$this->form_validation->set_rules('city', 'City', 'required|trim');
		$this->form_validation->set_rules('country', 'Country', 'required|trim');
		$this->form_validation->set_rules('address', 'Address', 'required|trim');

		$this->form_validation->set_rules(
			'password1',
			'Password',
			'required|trim|min_length[4]|matches[password2]',
			[
				'matches' => 'Password dont match !',
				'min_length' => 'Password too short !'
			]
		);
		$this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');

		if ($this->form_validation->run() == false) {
			$data['title'] = 'User Registration';
			$this->load->view('templates/auth_header', $data);
			$this->load->view('auth/registration');
			$this->load->view('templates/auth_footer');
		} else {
			$data = [
				'username' => htmlspecialchars($this->input->post('name'), true),
				'email' => htmlspecialchars($this->input->post('email'), true),
				'city' => htmlspecialchars($this->input->post('city'), true),
				'country' => htmlspecialchars($this->input->post('country'), true),
				'image' => 'default.jpg',
				'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
				'role_id' => 2,
				'is_active' => 0,
				'date_created' => time()
			];

			// $token = md5(mktime());
			// var_dump($token); die;

			$this->db->insert('register', $data);

			$this->_sendEmail();

			$this->session->set_flashdata(
				'message',
				'<div class="alert alert-success" role="alert">
			Coungratulation ! Your account has been created. Please login now !</div>'
			);
			redirect('auth');
		}
	}

private function _sendEmail(){

					$config = [
											'protocol' => 'smtp',
											'smtp_host' => 'ssl://smtp.googlemail.com',
											'smtp_user' => 'triosaputra308@gmail.com',
											'smtp_pass' => 'Smkn26jkt?',
											'smtp_port' => 25,
											'mailtype' => 'html',
											'charset' => 'utf-8',
											'newLine' => "\r\n"
					];
					$this->email->initialize($config);

					$this->email->from('triosaputra308@gmail.com', 'Trio saputra');
					$this->email->to('triosaputra308@gmail.com');
					$this->email->subject('Testing');
					$this->email->message('Hello World !');

					if($this->email->send()) {
						return true;
					} else{
						echo $this->email->print_debugger();
						die;
					}
}

	public function logout()
	{

		$this->session->unset_userdata('email');
		$this->session->unset_userdata('role_id');

		$this->session->set_flashdata(
			'message',
			'<div class="alert alert-success" role="alert">
			Log Out Success !</div>'
		);
		redirect('auth');
	}
	public function blocked()
	{
		echo "access blocked";
	}
}
