<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon">
            <i class="fab fa-free-code-camp"></i>
        </div>
        <div class="sidebar-brand-text mx-3"> ADMIN </div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- QUERY MENU -->
    <?php

    $role_id = $this->session->userdata('role_id');

    $query_Menu = " SELECT `user_menu`.`id`, `menu` 
                    FROM `user_menu` JOIN  `user_access_menu`
                    ON `user_menu`.`id` = `user_access_menu`.`menu_id`
                    WHERE `user_access_menu`.`role_id` = $role_id
                    ORDER BY `user_access_menu`.`menu_id` ASC
                ";
    $menu = $this->db->query($query_Menu)->result_array();

    ?>

    <!-- LOOPING MENU -->
    <?php foreach ($menu as $m) : ?>
        <div class="sidebar-heading">
            <?= $m['menu']; ?>
        </div>


        <!-- PREPARE SUB_MENU   -->

        <?php

        $menuId = $m['id'];
        $querySubMenu = " SELECT * 
                            FROM `user_sub_menu` JOIN `user_menu`
                            ON `user_sub_menu`.`menu_id` = `user_menu`.`id`
                            WHERE `user_sub_menu`.`menu_id` = $menuId
                            AND `user_sub_menu`.`is_active` = 1
        ";
        // BISA JUGA PAKE INI 
        // $querySubMenu = " SELECT *
        //                 FROM `user_sub_menu`
        //                 WHERE `menu_id` = $menuId
        //                 AND `user_sub_menu`.`is_active` = 1

        // ";

        $subMenu = $this->db->query($querySubMenu)->result_array();
        ?>

        <?php foreach ($subMenu as $sm) : ?>
            <?php if ($title == $sm['title']) : ?>
                <!-- Nav Item - Dashboard -->
                <li class="nav-item active">
                <?php else : ?>
                <li class="nav-item">
                <?php endif; ?>
                <a class="nav-link" href="<?php echo $sm['url']; ?>">
                    <i class="<?php echo $sm['icon']; ?>"></i>
                    <span><?php echo $sm['title'] ?></span></a>
            </li>

        <?php endforeach; ?>

        <hr class="sidebar-divider">
    <?php endforeach; ?>

    <!-- Nav Item - Charts -->
    <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url('auth/logout'); ?>">
            <i class="fas fa-fw fa-sign-out-alt"></i>
            <span> Logout </span>
        </a>
    </li>


    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->




<!-- how to debug -->
<!-- echo '<pre>' . print_r($sm['url'], true) . '</pre>'; -->