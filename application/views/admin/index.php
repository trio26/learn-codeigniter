<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?php echo $title?></h1>

</div>
<!-- /.container-fluid -->
<?php echo $this->session->flashdata('message'); ?>

<body>
    <table class="table table-hover col-lg-8">
        <thead>
            <tr>
                <th scope="col">No.</th>
                <th scope="col">Username</th>
                <th scope="col">Email</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <?php $i = 1;

        ?>
        <?php foreach ($data as $u) : ?>
            <tbody>
                <tr>
                    <th scope="row"><?php echo $i ?></th>
                    <td><?php echo $u['username']; ?></td>
                    <td><?php echo $u['email']; ?></td>
                    <td>
                        <a href="<?php echo base_url('admin/hapus/' . $u['customer_id']) ?>" class="badge badge-danger" onclick="return confirm('yakin ?')"> Delete </a>
                        <a href="<?php echo base_url() ?>" data-toggle="modal" data-target="#Createuser" class="badge badge-success"> Create </a>
                        <a href="" class="badge badge-warning" data-toggle="modal" data-target="#Showuser"> Show </a>
                    </td>
                </tr>
            </tbody>
            <?php $i++ ?>
        <?php endforeach; ?>

    </table>

</body>




<!-- Modal -->
<div class="modal fade" id="Showuser" tabindex="-1" role="dialog" aria-labelledby="ShowuserLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ShowuserLabel"> User data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?php echo base_url('admin'); ?>" method="post">
                <div class="ml-3 pt3">
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">Username</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" id="name" value="<?php echo $login['username'] ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10 pr2">
                            <input type="text" class="form-control" id="email" readonly="form-control" value="<?php echo $login['email'] ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="city" class="col-sm-2 col-form-label">City</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="city" readonly="form-control" value="<?php echo $login['city'] ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="country" class="col-sm-2 col-form-label">Contry</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="country" readonly="form-control" value="<?php echo $login['country'] ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="date" class="col-sm-2 col-form-label">Date Join</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="date" readonly="form-control" value="<?php echo date('d F Y'); ?>">
                        </div>
                    </div>
                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>



<!-- Modal create data -->
<!-- Modal -->
<div class="modal fade" id="Createuser" tabindex="-1" role="dialog" aria-labelledby="CreateuserLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="CreateuserLabel"> User data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?php echo base_url('admin'); ?>" method="post">
                <div class="ml-3 pt3">
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">Username</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" id="name" readonly="form-control" value="<?php echo $login['username'] ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10 pr2">
                            <input type="text" class="form-control" id="email" readonly="form-control" value="<?php echo $login['email'] ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="city" class="col-sm-2 col-form-label">City</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="city" readonly="form-control" value="<?php echo $login['city'] ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="country" class="col-sm-2 col-form-label">Contry</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="country" readonly="form-control" value="<?php echo $login['country'] ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="date" class="col-sm-2 col-form-label">Date Join</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="date" readonly="form-control" value="<?php echo date('d F Y'); ?>">
                        </div>
                    </div>
                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
