<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?php echo $title; ?></h1>

</div>
<!-- /.container-fluid -->

<div class="row">
    <div class="col-lg-8 pl-5">
        <?php echo form_open_multipart('user/edit'); ?>

        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
                <input type="email" name="email" value="<?php echo $login['email']; ?>" readonly class="form-control" id="inputEmail3" placeholder="Email">
            </div>
        </div>
        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">Full Name</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="username" placeholder="Username" value="<?php echo $login['username']; ?>">
                <?php echo form_error('username', '<small class="text-danger pl-3">', '</small>'); ?>

            </div>
        </div>
        <div class="form-group row">
            <div class="div col-sm-2"> Picture </div>
            <div class="div col-sm-10">
                <div class="row">
                    <div class="div col-sm-3">
                        <img src="<?php echo base_url('assets/profile/img/') . $login['image']; ?>" class="img-thumbnail">
                    </div>
                    <div class="div col-sm-9">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="image" name="image">
                            <label class="custom-file-label" for="image">Choose file</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group row justify-content-end">
            <div class="col-sm-10">
                <button type="submit" class="btn btn-primary"> Edit </button>
            </div>
        </div>


        </form>
    </div>
</div>