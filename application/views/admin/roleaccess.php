<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?php echo $title; ?></h1>

    <div class="row">
        <div class="col-lg-6">
            <h5> Role : <?php echo $role['role']; ?></h5>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">No.</th>
                        <th scope="col"> Menu </th>
                        <th scope="col"> Action</th>
                    </tr>
                </thead>
                <?php $i = 1; ?>
                <?php foreach ($menu as $m) : ?>
                    <tbody>
                        <tr>
                            <th scope="row"> <?php echo $i ?> </th>
                            <td><?php echo  $m['menu']; ?></td>
                            <td>
                                <div class="form-group form-check">
                                    <input type="checkbox" class="form-check-input" <?php echo check_access($role['id'], $m['id']); ?>>

                                </div>
                            </td>
                        </tr>
                    </tbody>
                    <?php $i++ ?>
                <?php endforeach; ?>
            </table>

        </div>

    </div>


</div>
<!-- /.container-fluid -->