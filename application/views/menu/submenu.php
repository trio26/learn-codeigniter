<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?php echo $title; ?></h1>

    <div class="row">
        <div class="col-lg">

            <?php if (validation_errors()) : ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo validation_errors(); ?> </div>
            <?php else : ?>
                <?php echo $this->session->flashdata('message'); ?>
            <?php endif; ?>

            <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#addNewSubMenu"> Add New Submenu</a>

            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">No.</th>
                        <th scope="col"> Title </th>
                        <th scope="col"> Menu </th>
                        <th scope="col"> Url </th>
                        <th scope="col"> Icon </th>
                        <th scope="col"> Status </th>
                        <th scope="col"> Action</th>
                    </tr>
                </thead>
                <?php $i = 1; ?>
                <?php foreach ($submenu as $m) : ?>
                    <tbody>
                        <tr>
                            <th scope="row"> <?php echo $i ?> </th>
                            <td><?php echo  $m['title']; ?></td>
                            <td><?php echo  $m['menu']; ?></td>
                            <td><?php echo  $m['url']; ?></td>
                            <td><?php echo  $m['icon']; ?></td>
                            <td><?php echo  $m['is_active']; ?></td>
                            <td>
                                <a href="" class="badge badge-success"> Edit </a>
                                <a href="" class="badge badge-danger"> Delete <a>
                            </td>
                        </tr>
                    </tbody>
                    <?php $i++ ?>
                <?php endforeach; ?>
            </table>

        </div>

    </div>


</div>
<!-- /.container-fluid -->


<!-- Modal -->
<div class="modal fade" id="addNewSubMenu" tabindex="-1" role="dialog" aria-labelledby="addNewSubMenuLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addNewSubMenuLabel">Add New Submenu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?php echo base_url('menu/submenu'); ?>" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control" id="title" name="title" placeholder="Mark title submenu">
                    </div>
                    <div class="form-group">
                        <select name="menu_id" id="menu_id" class="form-control">
                            <option value="">Select Menu </option>
                            <?php foreach ($menu as $m) : ?>
                                <option value="<?php echo $m['id'] ?>"> <?php echo $m['menu'] ?> </option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" id="url" name="url" placeholder="Mark url submenu">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="icon" name="icon" placeholder="Mark icon code submenu">
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="is_active" value="1" id="is_active" checked>
                            <label class="form-check-label" for="defaultCheck1">
                                Active submenu
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add Submenu</button>
                </div>
            </form>
        </div>
    </div>
</div>