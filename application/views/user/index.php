<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?php echo $title; ?></h1>

    <div class="div row">
        <div class="div col-lg-6">
            <?php echo $this->session->flashdata('message'); ?>
        </div>
    </div>
</div>
<!-- /.container-fluid -->

<!-- card element by bootstrap -->
<div class="card mb-3" style="max-width: 540px;">
    <div class="row no-gutters">
        <div class="col-md-4">
            <img src="<?php echo base_url('assets/profile/img/') . $login['image']; ?>" class="card-img" alt="...">
        </div>
        <div class="col-md-8">
            <div class="card-body">
                <h5 class="card-title"><?php echo $login['username']; ?></h5>
                <p class="card-text"><?php echo $login['email'] ?></p>
                <p class="card-text"><small class="text-muted">Member since <?php echo date('d F Y'); ?></small></p>
            </div>
        </div>
    </div>
</div>
</div>
<!-- End of Main Content -->