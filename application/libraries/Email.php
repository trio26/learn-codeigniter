class Email
{
        parent::Controller();
            $this->load->library('email');
}

public function index()
{
    $config['protocol']    = 'smtp';
    $config['smtp_host']    = 'ssl://smtp.gmail.com';
    $config['smtp_port']    = '465';
    $config['smtp_timeout'] = '7';
    $config['smtp_user']    = 'mygmail@gmail.com';
    $config['smtp_pass']    = '*******';
    $config['charset']    = 'utf-8';
    $config['newline']    = "\r\n";
    $config['mailtype'] = 'text'; // or html
    $config['validation'] = TRUE; // bool whether to validate email or not

    $this->email->initialize($config);


    $this->email->from('mygmail@gmail.com', 'myname');
    $this->email->to('target@gmail.com');

    $this->email->subject('Email Test');
    $this->email->message('Testing the email class.');

    $this->email->send();

    echo $this->email->print_debugger();

     $this->load->view('email_view');

   }
